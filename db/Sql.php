<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
	require("Conexion.php");	
	class Sql
	{
		private $PDOLocal;
		private $Rows;
		private $Datos=NULL;
		
		function __construct(){}
		
		function __destruct(){}
		
		function Conectar()
		{
			try
			{
				$StringConn=new Conexion();
		    	$this->PDOLocal=new PDO($StringConn->Servidor,$StringConn->Usuario,$StringConn->Pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				//PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
				//PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION
			}
			catch (Exception $e)
			{
				return "Error: ".$e->getMessage();
			}
			
						
		}
		
		function Desconectar()
		{
			$this->PDOLocal=NULL;
		}



		
		
		function Insert($Query)
		{
			try
			{
				//return $this->PDOLocal->exec($Query);
					$insert =$this->PDOLocal->prepare($Query)->execute();


					return $insert;//$insert->execute();
							
			}
			catch(Exception $e)
			{
				return "Error: ".$e->getMessage();
			}
		}
		
		function Select($Query)
		{
		      $this->Datos=array();
			try
			{
				$Resultado=$this->PDOLocal->query($Query);
				while($Row=$Resultado->fetch(PDO::FETCH_ASSOC))
				{
					
					$this->Datos[]=$Row;
				}
				
				return $this->Datos;			
				
			}
			catch(Exception $e)
			{
				
				return $this->Datos;
			}
		}
		
		function Delete($Query)
		{
			return $this->Insert($Query);
		}
		
		function Update($Query)
		{
			return $this->Insert($Query);
		}
		
		function SelectJson($Query)
		{
			
			return json_encode($this->Select($Query));
		}	
		
	}
?>