

<div class="modal fade" id="confModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">

    <div class="modal-content text-center">

      <div class="modal-header d-flex justify-content-center">
        <p class="heading">Confirmar</p>
      </div>

      <div class="modal-body">

        <i class="fa fa-bell fa-4x animated rotateIn mb-4"></i>

        <p>¿Seguro que desea expedir esta Factura?</p>

      </div>

      <div class="modal-footer flex-center">


        <button type="button" class="btn btn-success docto" onclick="docto(this.value)">Si</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>  
      </div>
    </div>
  </div>
</div>
