<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">                      
  <title>Facturación</title>                        
  <link rel="stylesheet" href="css/style.css">      
  <link rel="stylesheet" href="css/stylel.css">     
  <link rel="stylesheet" href="css/extras.css">     
   <link rel="stylesheet" href="css/bootstrap.min.css">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
   <script src="js/validate.js"></script>
</head>
<body>
  <div class="scene" id="events">
      <article class="event fullheight" id="businessmeetings">
        <div class="content">
          <h2><i class="fas fa-cube"></i>Facturación</h2>
          <ul id="event-list">
            <li>
            <span class="date">Sep 29</span><br>
            7:00 PM<br>
            Timbrados y actualización ante el SAT
            </li>
            
            <li>
            <span class="date">Sep 30</span><br>
            12:00 - 3:00 PM<br>
            Cargar archivos del SAT
            </li>
            
            <li>
            <span class="date">Oct 01</span><br>
            10:00 AM - 8:00 PM<br>
            Renovación de información de usuarios
            </li>
            
            <li>
            <span class="date">Oct 13 </span><br>
            2:00 - 6:00 PM<br>
            Repaso de informes
            </li>
          </ul>
          <div id="clock">
            <div class="label">Hoy</div>
            <div id="current-date"></div>
            <div id="current-time"></div>
          </div>
<ul id="event-list">
    <div class="col-xs-10 col-md-10 col-lg-10 col-sm-10">
        <div class="form-group">
        <div class="form" >
          <br>
          <form class="register-form" id="form">  
             <p class="login">Registrarse</p>
             <br>
                       <div class="input-group">
                           <input type="text"  maxlength="13" minlength="13" class="form-control" id="Rfc" name="Rfc" placeholder="RFC">
                       </div>
                       <br>
            <div class="input-group">
                           <input type="email"  class="form-control" id="email" name="email" placeholder="Correo">
                       </div>
                       <br>
            <div class="input-group">
                           <input type="password"  class="form-control" id="pass" name="pass" placeholder="Contraseña">
                       </div>
                       <br>
            <button type="button" id="btnreg" class="btn btn-success" value="Enviar">Enviar</button>
            <p class="message">Ya Registrado?: <a href="#">Login</a></p>
          </form>

          <form class="login-form">
            <p class="login">Login</p>
                      <br>
                       <div class="input-group">
                           <input type="text" maxlength="13"  class="form-control" name="user" placeholder="RFC">
                       </div>
                      <br>
                       <div class="input-group">
                           <input type="password"  class="form-control" name="pass" placeholder="Contraseña">
                       </div>
                       <br>
            <button type="button" id="btnlog" class="btn btn-success" value="Entrar">Entrar</button>
            <p class="message">Sin Registro?: <a href="#">Registrarse</a></p>
          </form>
        </div>
      </div>
     
    </div>
  </div>
</ul>
</article>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
      $('.message a').click(function(){
    $('form').animate({height:"toggle", opacity:"toggle"}, "slow");
  });
    </script>
   
  <script src="js/scriptl.js"></script>
  <script src="js/scripthora.js"></script>
</body>
</html>
