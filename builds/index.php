<?php
session_start();
require_once('../Modals/detalles.php');
require_once('../Modals/confirmExp.php');
// chdir('..');
// define('_CONTRASENA_','1006465601A-');
// require_once('db/Sql.php');
// include_once('service/tokenDB.php');
// echo $_SESSION['token_valid'];
// echo '<br>';


// print_r(jwt_decode($_SESSION['token_valid'],_CONTRASENA_));
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script defer src="js/lib/fontawesome-all.min.js"></script>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <title>Facturación</title>
</head>

<body data-spy="scroll" data-target=".site-nav" data-offset="55">
  <header id="page-hero" class="site-header">
    <nav class="site-nav family-sans text-uppercase navbar navbar-expand-md navbar-dark fixed-top">
      <div class="container-fluid">
        <a class="navbar-brand" href="#page-hero">
          <i class="fas fa-cube"></i> Factura</a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#myTogglerNav" aria-controls="#myTogglerNav"
          aria-label="Toggle Navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <section class="collapse navbar-collapse" id="myTogglerNav">
          <div class="navbar-nav ml-auto">
            <a class="nav-item nav-link" href="#page-hero">Home</a>
            <a class="nav-item nav-link" href="#page-multicolumn">Perfil</a>
            <a class="nav-item nav-link" href="#page-media">Facturación</a>
            <a class="nav-item nav-link" href="https://www.sat.gob.mx/home#">Portal del SAT</a>
          </div>
        </section>
      </div>
    </nav>
    <section class="layout-hero d-flex align-items-center text-light text-center">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-11 col-sm-10 col-md-8 animated fadeInUp">
            <h3 class="page-section-title">Sistema de Facturación</h3>
              <p class="page-section-text">Todo lo que buscas en un solo lugar.
            </p>
          </div>
        </div>
      </div>
    </section>
  </header>
  <article id="page-multicolumn" class="page-section text-center py-5">
    <header class="page-section-header container">
      <div class="row justify-content-center">
        <div class="col-lg-6 col-sm-6 col-md-3">
          <br>
          <h2 class="page-section-title">Perfil</h2>
          <br>
        </div>
      </div>
    </header>
    <section class="layout-multicolumn container">
      <form id="form">
      <div class="row justify-content-center family-sans text-uppercase">

        <section class="mb-5 col-12 col-md-6 col-lg-4 col-xl-3">
          <a data-toggle="modal" data-target="#site-modal" data-highres="images/man.svg" href="#">
            <img class="layout-image" src="images/man.svg" alt="usuario">
          </a>
          <h5 class="layout-title pt-2">editar imagen</h5>
        </section>

        <section class= " col-lg-6 col-sm-6 col-md-3">
          <div class="form-group">
          <input disabled type="text" class="form-control" name="Incidente" placeholder="Nombre">
          <br>
          <input disabled type="text" class="form-control" name="rfc" placeholder="RFC">
          <br>
          <input disabled type="text" class="form-control" name="email" placeholder="Correo">
          <br>
          <input disabled type="text" class="form-control" name="pass" placeholder="Contraseña">
          <br>
          <input disabled type="text" class="form-control" name="pass2" placeholder="Confirmar Contraseña">
        </div>
      </section>
      </div>
    </form>
      <div class="form-group">
          <button type="button" class="btn btn-success pull-right" value="Guardar" id="btnsavbtnguardar">Guardar</button>
          <button type="button" class="btn btn-danger pull-right" value="Cancelar" id="btncancelar">Cancelar</button>
          <button type="button" class="btn btn-primary pull-right" value="Editar" id="btneditar">Editar</button>
        </div>
  </section>
  </article>
    <article id="page-media" class="page-section py-5">
    <section class="layout-media container my-5">
      <div class="row justify-content-center">
        <div class="col-lg-12 layout-animation invisible">
          <section class="media">
            <div class="media-body">
              <article id="page-media" class="page-section text-center py-5">
                <header class="page-section-header container text-center">
                 <h2 class="page-section-title">Buscar Factura</h2>
               </header>    
              </article>

          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Facturas por Expedir</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Facturas Expedidas</a>
            </li>
          </ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

<div id="main-app">
  <div class="add-appointment card textcenter mt-3">
    <div class="apt-addheading card-header bg-dark text-white">
      <i class="fas fa-plus"></i> Más Opciones
    </div><!-- card-header git-->
    <div class="card-body style=display:none;">
      <form id="aptForm">
        <div class="form-group form-row">
          <label class="col-md-2 col-form-label text-md-right" for="petName">Catalogo</label>
          <div class="col-md-10">
            <select type="text" class="form-control" name="petName" id="petName" placeholder="Folio">
              <option value="seleccione uno">seleccione uno</option>
               <option value="Producto">Producto</option>
                <option value="seleccione uno">Servicio</option>
            </select> 
          </div>
        </div>
        <div class="form-group form-row">
          <label class="col-md-2 col-form-label text-md-right"
            for="ownerName">Razón Social</label>
          <div class="col-md-10">
            <input type="text" class="form-control"
              id="ownerName" placeholder="Razón Social">
          </div>
        </div>

        <div class="form-group form-row">
          <label class="col-md-2 col-form-label text-md-right" for="aptDate">Fecha</label>
          <div class="col-md-4">
            <input type="date" class="form-control" id="aptDate">
          </div>
          <label class="col-md-2 col-form-label text-md-right" for="aptTime">Hora</label>
          <div class="col-md-4">
            <input type="time" class="form-control" name="aptTime" id="aptTime">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
  <div class="search-appointments row justify-content-center my-4">
    <div class="col-md-6">
      <div class="input-group">
        <button type="submit" disabled class="btn btn-default d-block ml-auto">Facturas por Expedir</button>
        <input type = "text" name="inbuscar" id="inbuscar" placeholder="Folio" class="form-control">
        </div> 
      </div>
    </div>
     <div id="result"></div>
       </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
          <div id="main-app">
  <div class="add-appointment card textcenter mt-3">
    <div class="apt-addheading card-header bg-dark text-white">
      <i class="fas fa-plus"></i> Más Opciones
    </div>
    <div class="card-body style=display:none;"> 
      <form id="aptForm">
        <div class="form-group form-row">
          <label class="col-md-2 col-form-label text-md-right" for="petName">Catalogo</label>
          <div class="col-md-10">
            <select type="text" class="form-control" name="petName" id="petName" placeholder="Folio">
              <option value="seleccione uno">seleccione uno</option>
               <option value="Producto">Producto</option>
                <option value="seleccione uno">Servicio</option>
            </select> 
          </div>
        </div>
        <div class="form-group form-row">
          <label class="col-md-2 col-form-label text-md-right"
            for="ownerName">Razón Social</label>
          <div class="col-md-10">
            <input type="text" class="form-control"
              id="ownerName" placeholder="Razón Social">
          </div>
        </div>

        <div class="form-group form-row">
          <label class="col-md-2 col-form-label text-md-right" for="aptDate">Fecha</label>
          <div class="col-md-4">
            <input type="date" class="form-control" id="aptDate">
          </div>
          <label class="col-md-2 col-form-label text-md-right" for="aptTime">Hora</label>
          <div class="col-md-4">
            <input type="time" class="form-control" name="aptTime" id="aptTime">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>  
  <div class="search-appointments row justify-content-center my-4">
    <div class="col-md-6">
      <div class="input-group">
        <button type="submit" disabled class="btn btn-default d-block ml-auto">Facturas Expedidas</button>
        <input type = "text" name="inbuscar2" id="inbuscar2" placeholder="Folio" class="form-control">
        </div> 
      </div>
    </div>
     <div id="result2"></div>
        </div>
</div>
          </section>
        </div>
      </div>
    </section>

  </article>
 
  <footer class="site-footer bg-dark text-light d-flex justify-content-center">
  <article class="content">
    <div id="socialmedia">
      <ul class="group">
        <p style="color:white; padding-top:11px;">Sistema de Facturas - 2018</p>   
      </ul>      
    </div>
  </article>
</footer>

  <article id="site-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <img data-dismiss="modal" class="img-fluid" src="#" alt="Modal Photo">
      </div>
    </div>
  </article>


 

  <script src="js/lib/jquery.min.js"></script>

  <script src="js/lib/jsquery.min.js"></script>
  <script src="js/lib/jsquery.loadTemplate.min.js"></script>
  <script src="js/lib/jquerys-dateFormat.min.js"></script>
  <script src="js/lib/poppers.min.js"></script>
  <script src="js/lib//bootstrap.min.js"></script>
  <script src="js/lib/bootstrap.bundle.min.js"></script>
  <script src="js/lib/underscore-min.js"></script>
  <script src="js/script.js"></script>
  <script src="js/scripts.js"></script>
  <script src="js/lib/bootstrap-confirmation.js"></script>
</body>
<script>
  $(document).ready(function()
  {
   $('#inbuscar').keyup(function()  
    {
        var txt = $(this).val();
        if(txt!=''){
        $.ajax
        ({
          url: "../service/folio.php",
          method: "POST",
          data: {search:txt},
          dataType: "text",
          success: function(data)
          {
            $('#result').html(data);
          }
        });
      }
      else{
        $('#result').html("");
      }
    });
  });
</script>

<script>
  $(document).ready(function()
  {
   $('#inbuscar2').keyup(function()  
    {
        var txt = $(this).val();
        if(txt!=''){
        $.ajax
        ({
          url: "../service/folioex.php",
          method: "POST",
          data: {search:txt},
          dataType: "text",
          success: function(data)
          {
            $('#result2').html(data);
          }
        });
      }
      else{
        $('#result2').html("");
      }
    });
  });
</script>
</html>