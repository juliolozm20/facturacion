<?php

chdir('..');

require_once('db/Sql.php');

$_con=new Sql();
$_con->Conectar();


session_start();
$IdFactura=isset($_GET['IdFactura'])?$_GET['IdFactura']:'';
$Docto_Ve=isset($_GET['Docto_Ve'])?$_GET['Docto_Ve']:'';
$Folio=isset($_GET['Folio'])?$_GET['Folio']:'';
$Moneda_Id=isset($_GET['Moneda_Id'])?$_GET['Moneda_Id']:'';
$Fecha=isset($_GET['Fecha'])?$_GET['Fecha']:'';
$Importe_Neto=isset($_GET['Importe_Neto'])?$_GET['Importe_Neto']:'';
$Total_Impuestos=isset($_GET['Total_Impuestos'])?$_GET['Total_Impuestos']:'';
$Pctje_Impuesto=isset($_GET['Pctje_Impuesto'])?$_GET['Pctje_Impuesto']:'';
$Activo=isset($_GET['Activo'])?$_GET['Activo']:'';
$Expedida=isset($_GET['Expedida'])?$_GET['Expedida']:'';
$output='';

if (isset($_POST["search"])){
$sql = "SELECT facturas.IdFactura, facturas.Folio, CASE WHEN facturas.Moneda_Id = 1 THEN 'MX'
             						WHEN facturas.Moneda_Id = 20030 THEN 'USD' ELSE 0 END as Moneda_Id,
               facturas.Docto_Ve,facturas.Fecha, facturas.Importe_Neto, facturas.Total_Impuestos, imp.Pctje_Impuesto, facturas.Activo, facturas.Expedida
		FROM facturas
		INNER JOIN impuestos as imp ON imp.Docto_Ve = facturas.Docto_Ve
		Where facturas.Folio LIKE '%".$_POST["search"]."%' and facturas.Expedida=0";


$_con->Select($sql);

$result =$_con->Select($sql);
$output .='<h4 align="center">Resultados</h4>';
	$output .='<div class="table-responsive" id="fol">
				<table class="table table-responsive table-hover">
				<thead>
				<tr>
					<th></th>
					<th>Folio</th>
					<th>Moneda</th>
					<th>Fecha</th>
					<th>Importe Neto</th>
					<th>Total Impuestos</th>
					<th>% Impuesto</th>
					<th>Conceptos</th>
					<th>Expedir</th>
				</tr>
				</thead>';

foreach ($_con->Select($sql)as $row) {
	$output .= '
				<tr>
				<td><img src="\facturacionmicrosip\img\clock.png" alt="expirado"></td>
					<td>'.$row["Folio"].'</td>
					<td>'.$row["Moneda_Id"].'</td>
					<td>'.$row["Fecha"].'</td>
					<td>'.$row["Importe_Neto"].'</td> 
					<td>'.$row["Total_Impuestos"].'</td>
					<td>'.$row["Pctje_Impuesto"].'</td>
					<td><button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#detModal" onclick="doct('.$row["Docto_Ve"].') ;">Conceptos</button></td>
					
					<td><button type="button" class="btn btn-warning pull-right doctos" data-toggle="modal" data-target="#confModal">Expedir Factura</a></td>
				</tr>';
}

echo $output;
}

?>
