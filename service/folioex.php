<?php

chdir('..');

require_once('db/Sql.php');

$_con=new Sql();
$_con->Conectar();


session_start();

$Docto_Ve=isset($_GET['Docto_Ve'])?$_GET['Docto_Ve']:'';
$Folio=isset($_GET['Folio'])?$_GET['Folio']:'';
$Moneda_Id=isset($_GET['Moneda_Id'])?$_GET['Moneda_Id']:'';
$Fecha=isset($_GET['Fecha'])?$_GET['Fecha']:'';
$Importe_Neto=isset($_GET['Importe_Neto'])?$_GET['Importe_Neto']:'';
$Total_Impuestos=isset($_GET['Total_Impuestos'])?$_GET['Total_Impuestos']:'';
$Pctje_Impuesto=isset($_GET['Pctje_Impuesto'])?$_GET['Pctje_Impuesto']:'';
$output='';


$sql = "SELECT facturas.Folio, CASE WHEN facturas.Moneda_Id = 1 THEN 'MX'
             						WHEN facturas.Moneda_Id = 20030 THEN 'USD' ELSE 0 END as Moneda_Id,
               facturas.Docto_Ve,facturas.Fecha, facturas.Importe_Neto, facturas.Total_Impuestos, imp.Pctje_Impuesto
		FROM facturas
		INNER JOIN impuestos as imp ON imp.Docto_Ve = facturas.Docto_Ve
		Where facturas.Folio LIKE '%".$_POST["search"]."%' and Expedida=1";

$_con->Select($sql);
$result =$_con->Select($sql);
$output .='<h4 align="center">Resultados</h4>';
	$output .='<div class="table-responsive">
				<table class="table table-responsive table-hover">
				<thead>
				<tr>
					<th>Folio</th>
					<th>Moneda</th>
					<th>Fecha</th>
					<th>Importe Neto</th>
					<th>Total Impuestos</th>
					<th>% Impuesto</th>
					<th>Conceptos</th>
					</tr>
				</thead>';
foreach ($_con->Select($sql)as $row) {
	
	$output .= '
				<tr>
					<td>'.$row["Folio"].'</td>
					<td>'.$row["Moneda_Id"].'</td>
					<td>'.$row["Fecha"].'</td>
					<td>'.$row["Importe_Neto"].'</td>
					<td>'.$row["Total_Impuestos"].'</td>
					<td>'.$row["Pctje_Impuesto"].'</td>
					<td><button type="button" class="btn btn-info pull-right conc" data-toggle="modal view_data" data-target="#detModal" name="view" onclick="doct('.$row["Docto_Ve"].') ;">Conceptos</button></td>
					
				</tr>';
}

echo $output;

?>
<script>
	function doct (Docto_Ve_Id) {

		$.ajax
        ({
          url: "../service/concepto.php",
          method: "POST",	
          data: {Docto_Ve:Docto_Ve_Id},
          success: function(data)
          {
            $('#results').html(data);
            $('#detModal').modal('show');
          }
        });
	}
  // $(document).on('click', '.conc', function()
  // {  
  //       var Docto_Ve = $(this).attr("Docto_Ve_Id");
  //       $.ajax
  //       ({
  //         url: "../service/concepto.php",
  //         method: "POST",	
  //         data: {Docto_Ve:Docto_Ve_Id},
  //         success: function(data)
  //         {
  //           $('#results').html(data);
  //           $('#detModal').modal('show');
  //         }
  //       });
  // });
</script>	